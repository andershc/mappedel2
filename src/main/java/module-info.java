module org.ntnu.andershc.mappedel2 {
    requires javafx.controls;
    requires javafx.fxml;

    opens org.ntnu.andershc.mappedel2.model to javafx.fxml,java.base,javafx.graphics;
    opens org.ntnu.andershc.mappedel2.controllers to javafx.fxml,java.base,javafx.graphics;
    opens org.ntnu.andershc.mappedel2.views to javafx.fxml,java.base,javafx.graphics;

    exports org.ntnu.andershc.mappedel2;
    exports org.ntnu.andershc.mappedel2.model;
    exports org.ntnu.andershc.mappedel2.controllers;
    exports org.ntnu.andershc.mappedel2.views;

}
