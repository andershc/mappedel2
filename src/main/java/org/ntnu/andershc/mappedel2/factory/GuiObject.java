package org.ntnu.andershc.mappedel2.factory;

import javafx.scene.Node;

/**
 * Abstract class for the gui objects.
 */
abstract class GuiObject {

    abstract Node createObject();
}
