package org.ntnu.andershc.mappedel2.factory;

import javafx.scene.Node;
import javafx.scene.control.TableView;
import org.ntnu.andershc.mappedel2.model.Patient;

public class TableViewObject extends GuiObject{
    /**
     * Generates a table view
     */
    @Override
    public Node createObject() {
        TableView<Patient> tableView = new TableView();

        return tableView;
    }
}
