package org.ntnu.andershc.mappedel2.factory;

import javafx.scene.Node;
import javafx.scene.control.ToolBar;

public class ToolBarObject extends GuiObject{
    /**
     * Generates a toolbar
     */
    @Override
    public Node createObject() {
        ToolBar toolBar = new ToolBar();

        return toolBar;
    }
}
