package org.ntnu.andershc.mappedel2.factory;

import javafx.scene.Node;
import javafx.scene.control.MenuBar;

public class MenuBarObject extends GuiObject{
    /**
     * Generates a menu bar.
     */
    @Override
    public Node createObject() {
        MenuBar menuBar = new MenuBar();

        return menuBar;
    }
}
