package org.ntnu.andershc.mappedel2.factory;

/**
 * Factory to create gui objects.
 */
public class GuiFactory {
    /**
     * Use getGuiObject to get gui object
     */
    public GuiObject getGuiObject(String guiObjectType){
        if(guiObjectType == null){
            return null;
        }
        if(guiObjectType.equalsIgnoreCase("MENUBAR")){
            return new MenuBarObject();
        } else if(guiObjectType.equalsIgnoreCase("TABLEVIEW")){
            return new TableViewObject();
        } else if(guiObjectType.equalsIgnoreCase("TOOLBAR")){
            return new ToolBarObject();
        }
        return null;
    }
}
