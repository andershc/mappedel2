package org.ntnu.andershc.mappedel2.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.ntnu.andershc.mappedel2.App;
import org.ntnu.andershc.mappedel2.exception.RemoveException;
import org.ntnu.andershc.mappedel2.fileHandling.Reader;
import org.ntnu.andershc.mappedel2.fileHandling.Writer;
import org.ntnu.andershc.mappedel2.model.Patient;
import org.ntnu.andershc.mappedel2.model.PatientRegister;
import org.ntnu.andershc.mappedel2.views.FileTypeDialog;
import org.ntnu.andershc.mappedel2.views.NothingSelectedAlert;
import org.ntnu.andershc.mappedel2.views.PatientDialog;

import java.io.File;
import java.util.Optional;

/**
 * Controller class for the main view, and main fxml file.
 */
public class MainController{

    public Label status;
    @FXML
    private TableView<Patient> tableView;
    @FXML
    private TableColumn<Patient, String> lastNameColumn;
    @FXML
    private TableColumn<Patient, String> firstNameColumn;
    @FXML
    private TableColumn<Patient, String> socialSecurityNumber;
    @FXML
    private TableColumn<Patient, String> diagnosis;
    @FXML
    private TableColumn<Patient, String> generalPractitioner;

    /**
     * Method that runs when the window is opened up.
     * Calls other methods to set the content of the table view.
     */
    public void initialize() {
        tableView.setEditable(true);
        updateList();
        columFactory();
        status.setText("application initialized...");
    }

    /**
     * Method is called when the add new patient buttons are pressed.
     * Creates a new PatientDialog and sets the register.
     */
    @FXML
    public void addNewPatient(){
        PatientDialog patientDialog = new PatientDialog(App.getRegister());
        while (true){
            Optional<Boolean> result = patientDialog.showAndWait();
            if(result.isPresent() && result.get()){
                updateList();
                break;
            }
        }

    }

    /**
     *  Method is called when the edit selected patient buttons are pressed.
     *  If no patient is selected, an alert is shown.
     *  Creates a new PatientDialog and sets the register.
     */
    @FXML
    public void editPatient(){
        Patient existingPatient = tableView.getSelectionModel().getSelectedItem();
        if(existingPatient == null){
            NothingSelectedAlert alert = new NothingSelectedAlert(Alert.AlertType.INFORMATION);
            alert.showAndWait();
        }else{
            PatientDialog patientDialog = new PatientDialog(App.getRegister(), existingPatient);
            while (true){
                Optional<Boolean> result = patientDialog.showAndWait();
                if(result.isPresent() && result.get()){
                    App.resetWrapper();
                    updateList();
                    break;
                }
            }
        }
    }

    /**
     * Method is called when remove patient button is pressed.
     * If there are no patients selected,an alert is shown.
     * Shows a confirmation alert to confirm the action.
     * @throws RemoveException if the person isn't found in the list.
     */
    @FXML
    public void removePatient() throws RemoveException {
        Patient existingPatient = tableView.getSelectionModel().getSelectedItem();
        if(existingPatient == null){
            NothingSelectedAlert alert = new NothingSelectedAlert(Alert.AlertType.INFORMATION);
            alert.showAndWait();
        }else {
            PatientRegister register = App.getRegister();
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Confirmation Dialog - Delete Item");
            alert.setContentText("Are you sure you want to delete this item?");

            Optional<ButtonType> result = alert.showAndWait();
            if (result.isPresent() && result.get() == ButtonType.OK) {
                register.remove(existingPatient.getSocialSecurityNumber());
                App.setRegister(register);
            }
            App.resetWrapper();
            updateList();
        }
    }

    /**
     * A method to update the table view and the register.
     */
    private void updateList(){
        App.updateWrapper();
        tableView.setItems(App.getWrapper());
    }

    /**
     * Initializes the columns of the tableview.
     */
    private void columFactory(){
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        socialSecurityNumber.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));
        diagnosis.setCellValueFactory(new PropertyValueFactory<>("diagnosis"));
        generalPractitioner.setCellValueFactory(new PropertyValueFactory<>("generalPractitioner"));
    }

    /**
     * Called when pressing the about button.
     * An alert is shown displaying information about the application.
     */
    @FXML
    public void showAbout() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.initModality(Modality.APPLICATION_MODAL);
        alert.setTitle("Information Dialog - About");
        alert.setHeaderText("Patient Register\nv0.1-SNAPSHOT");
        alert.setContentText("A brilliant application created by" +
                "\n\u00A9Anders Heftøy Carlsen\n2021-04-30 ");
        alert.showAndWait();

    }

    /**
     * Method is called when pressing Import .CSV...
     * Let's you choose a file to import, the file must be a .csv file.
     * If the file selected is not .csv a dialog is shown.
     */
    @FXML
    public void importCSV() {
        Stage stage = new Stage();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select a file");
        File file = fileChooser.showOpenDialog(stage);
        if (file != null) {
            while (true) {
                Reader reader = new Reader(file.getPath());
                try {
                    reader.readCSV(App.getRegister());
                    updateList();
                    status.setText("imported successfully...");
                    break;
                } catch (IllegalArgumentException e) {
                    status.setText("import failed...");
                    FileTypeDialog fileTypeDialog = new FileTypeDialog();
                    fileTypeDialog.setHeaderText(e.getMessage());
                    Optional<File> result = fileTypeDialog.showAndWait();
                    if (result.isPresent()) {
                        file = result.get();
                    }
                }
            }
        }
    }
    /**
     * Method is called when pressing Export .CSV...
     * Let's you export the register to a .csv file
     */
    @FXML
    public void exportCSV(){
        Stage stage = new Stage();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save");
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter(".csv", "*.csv"));
        File file = fileChooser.showSaveDialog(stage);

        if(file != null) {
            Writer writer = new Writer(file.getPath());
            writer.writeRegisterCSV(App.getRegister());

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.initModality(Modality.APPLICATION_MODAL);
            alert.setTitle("Information Dialog - Export");
            alert.setHeaderText("Changes saved to " + file.getPath());
            alert.showAndWait();
            status.setText("file exported successfully...");
        }
    }
    /**
     * Called when pressing exit
     */
    @FXML
    private void exit(){
        App.alertOnExit();
    }
}
