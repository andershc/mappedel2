package org.ntnu.andershc.mappedel2.views;

import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import org.ntnu.andershc.mappedel2.exception.AddException;
import org.ntnu.andershc.mappedel2.model.Patient;
import org.ntnu.andershc.mappedel2.model.PatientRegister;

import java.util.Optional;

/**
 * Dialog class for adding or editing a Patient.
 */
public class PatientDialog extends Dialog<Boolean> {

    private final Mode mode;
    private PatientRegister register;
    private Patient existingPatient;

    /**
     * Enumerator for class
     */
    public enum Mode{
        ADD, EDIT
    }
    /**
     * Constructor for PatientDialog
     */
    public PatientDialog(PatientRegister register) {
        super();
        this.register = register;
        this.mode = Mode.ADD;
        // Create the content of the dialog
        createPatient();
    }
    /**
     * Constructor for PatientDialog
     * @param patient patient to edit
     */
    public PatientDialog(PatientRegister register, Patient patient) {
        super();
        this.mode = Mode.EDIT;
        this.register = register;
        this.existingPatient = patient;
        // Create the content of the dialog
        createPatient();
    }

    /**
     * Creates the content for dialog depending on the Mode
     */
    private void createPatient() {
        // Set title depending upon mode...
        switch (this.mode) {
            case EDIT:
                setTitle("Task Details - Edit");
                break;

            case ADD:
                setTitle("Task Details - Add");
                break;

            default:
                setTitle("Patient Details - UNKNOWN MODE...");
                break;

        }

        // Set the button types.
        getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);

        VBox mainBox = new VBox();
        GridPane gridPane = new GridPane();

        TextField firstName = new TextField();
        firstName.setPromptText("First name:");
        TextField lastName = new TextField();
        lastName.setPromptText("Last name:");
        TextField socialSecurityNumber = new TextField();
        socialSecurityNumber.setPromptText("Social security number:");
        socialSecurityNumber.textProperty().addListener((observableValue, oldValue, newValue) -> {
            if(!newValue.matches("\\d")){
                socialSecurityNumber.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });
        TextField generalPractitioner = new TextField();
        generalPractitioner.setPromptText("General practioner:");
        TextField diagnosis = new TextField();
        diagnosis.setPromptText("Diagnosis:");


        if (mode == Mode.EDIT) {
            firstName.setText(existingPatient.getFirstName());
            lastName.setText(existingPatient.getLastName());
            socialSecurityNumber.setText(existingPatient.getSocialSecurityNumber());
            socialSecurityNumber.setDisable(true);
            generalPractitioner.setText(existingPatient.getGeneralPractitioner());
            diagnosis.setText(existingPatient.getDiagnosis());
        }

        //Add GridPane with labels and text fields
        gridPane.add(new Label("First name:"), 0,0);
        gridPane.add(new Label("Last name:"), 0, 1);
        gridPane.add(new Label("Social security number:"), 0, 2);
        gridPane.add(new Label("Diagnosis"),0, 3);
        gridPane.add(new Label("General practitioner:"),0, 4);

        gridPane.add(firstName, 1, 0);
        gridPane.add(lastName, 1, 1);
        gridPane.add(socialSecurityNumber, 1, 2);
        gridPane.add(diagnosis,1 ,3);
        gridPane.add(generalPractitioner,1 , 4);
        mainBox.getChildren().add(gridPane);
        if(mode == Mode.EDIT){
            mainBox.getChildren().add(new Text("\nIt's not possible to change the social security number.\n" +
                    "If you want to change it, please delete the patient and add a new one."));
        }
        getDialogPane().setContent(mainBox);

        //Does the requested action depending on the button pressed
        setResultConverter((ButtonType button) -> {
            Boolean result = true;
            if (button == ButtonType.OK) {
                if (mode == Mode.ADD) {
                    try {
                        register.addPatient(firstName.getText(), lastName.getText(), socialSecurityNumber.getText(),
                                diagnosis.getText(), generalPractitioner.getText());
                    } catch (AddException | IllegalArgumentException | NullPointerException e) {
                        Alert alert = new Alert(Alert.AlertType.WARNING);
                        alert.setTitle("Can't add patient.");
                        alert.setHeaderText(e.getMessage());
                        alert.setContentText("Please add the patient again");
                        Optional<ButtonType> check = alert.showAndWait();
                        if(check.isPresent() && check.get() == ButtonType.OK){
                           result = false;
                        }
                    }
                } else if (mode == Mode.EDIT) {
                    try {
                        register.editPatient(firstName.getText(), lastName.getText(), socialSecurityNumber.getText(),
                                diagnosis.getText(), generalPractitioner.getText());
                    } catch(NullPointerException e){
                        Alert alert = new Alert(Alert.AlertType.WARNING);
                        alert.setTitle("Can't edit patient.");
                        alert.setHeaderText(e.getMessage());
                        alert.setContentText("Please edit the patient again.");
                        Optional<ButtonType> check = alert.showAndWait();
                        if(check.isPresent() && check.get() == ButtonType.OK){
                            result = false;
                        }
                    }
                }
            }
            return result;
        });
    }
}