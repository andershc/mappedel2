package org.ntnu.andershc.mappedel2.views;

import javafx.scene.control.Alert;
    /**
     * Class for showing an alert when nothing is selected when trying
     * to remove or edit a patient.
     */
public class NothingSelectedAlert extends Alert {
    /**
     * Constructor for the alert class.
     * @param alertType what type of alert should be shown.
     */
    public NothingSelectedAlert(AlertType alertType) {
        super(alertType);
        createAlert();
    }

    /**
     * Creates the objects in the alert.
     */
    private void createAlert(){
        setTitle("Select a patient:");
        setHeaderText("No patient selected" );
        setContentText("Please select a patient.");
    }
}
