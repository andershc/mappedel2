package org.ntnu.andershc.mappedel2.fileHandling;

import org.ntnu.andershc.mappedel2.model.Patient;
import org.ntnu.andershc.mappedel2.model.PatientRegister;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Class for writing a .csv file.
 */
public class Writer {
    /**
     * Path to write to
     */
    private final String writePath;
    /**
     * Constructor for class, sets the path
     * @param writePath Path were file is written
     */
    public Writer(String writePath){
        this.writePath = writePath;
    }

    /**
     * Method for writing the register to a .csv file
     */
    public void writeRegisterCSV(PatientRegister register) {
        if(!writePath.contains(".csv")){
            throw new IllegalArgumentException("Path must be to a .csv file");
        }else {
            PrintWriter printWriter = null;
            try {
                BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(writePath, false));
                printWriter = new PrintWriter(bufferedWriter);
                printWriter.println("firstName;lastNam;generalPractitioner;socialSecurityNumber");
                for (Patient p : register.getAllPatients().values()) {
                    String string = p.getFirstName() + ";" +
                            p.getLastName() + ";" +
                            p.getGeneralPractitioner() + ";" +
                            p.getSocialSecurityNumber() + ";" +
                            p.getDiagnosis();
                    printWriter.println(string);
                }

            } catch (IOException i) {
                System.out.println(i.getMessage());
            } finally { //The files will close even tough an exception will be caught when writing a file.
                try {
                    printWriter.close();
                } catch (NullPointerException e) {
                    System.out.println(e.getMessage());
                }
            }
        }
    }
}
