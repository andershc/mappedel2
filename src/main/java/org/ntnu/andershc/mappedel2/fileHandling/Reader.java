package org.ntnu.andershc.mappedel2.fileHandling;

import org.ntnu.andershc.mappedel2.exception.AddException;
import org.ntnu.andershc.mappedel2.model.PatientRegister;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Class to Read .csv files.
 */
public class Reader {
    private String path;
    private String line;

    /**
     * Constructor for reader, takes in the path of the file
     * to be read.
     * @param path
     */
    public Reader(String path){
        this.path = path;
        line = "";
    }

    /**
     * Method that reads the .csv file and adds the data to the register.
     * @param register
     */
    public void readCSV(PatientRegister register) {
        if(!path.endsWith(".csv")) {
            throw new IllegalArgumentException("The chosen file type is not valid.");
        } else {
            BufferedReader bufferedReader = null;
            try {
                bufferedReader = new BufferedReader(new FileReader(path));
                while ((line = bufferedReader.readLine()) != null) {
                    String[] values = line.split(";");
                    try {
                        if (values.length == 4) {
                            register.addPatient(values[0], values[1], values[3], "-", values[2]);
                        } else if (values.length == 5) {
                            register.addPatient(values[0], values[1], values[3], values[4], values[2]);
                        }
                    } catch (AddException | IllegalArgumentException e) {
                        System.out.println(e.getMessage());
                    }

                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally { //The files will close even tough an exception will be caught when reading from task files.
                try {
                    bufferedReader.close();
                } catch (IOException i) {
                    System.out.println(i.getMessage());
                }
            }
        }
    }
}