package org.ntnu.andershc.mappedel2.model;

import org.ntnu.andershc.mappedel2.exception.AddException;
import org.ntnu.andershc.mappedel2.exception.RemoveException;

import java.util.HashMap;

/**
 * Patient register handles the patients and the register.
 */
public class PatientRegister {
    private HashMap<String, Patient> patients;

    public PatientRegister() {
        patients = new HashMap<>();
    }
    /**
     * Adds a patient to the patients list if the patient doesn't exist already
     */
    public void addPatient(String firstname, String lastName,
                           String socialSecurityNumber, String diagnosis,
                           String generalPractitioner) throws AddException {
        if(!socialSecurityNumber.matches("[0-9]+") || socialSecurityNumber.length() != 11){
            throw new IllegalArgumentException("Social security number must be 11 numbers.");
        } else if(firstname.equals("") || lastName.equals("")){
            throw new NullPointerException("Name cannot be null");
        } else if(!patients.containsKey(socialSecurityNumber)){
            patients.put(socialSecurityNumber,
                    new Patient(firstname, lastName, socialSecurityNumber, diagnosis, generalPractitioner));
        }
        else{
            throw new AddException(socialSecurityNumber + " is already registered.");
        }
    }
    /**
     * Removes a person from either employees or patients
     * @throws RemoveException if the person isn't found in the list.
     */
    public void remove(String socialSecurityNumber) throws RemoveException {
        if(socialSecurityNumber.equals("")){
            throw new NullPointerException("Input can't be null");
        }
        else if(patients.containsKey(socialSecurityNumber)){
            patients.remove(socialSecurityNumber);
        }
        else{
            throw new RemoveException(socialSecurityNumber + " does not exist in the register");
        }
    }

    public HashMap<String, Patient> getAllPatients(){
        HashMap<String, Patient> allPatients = new HashMap<>();
        for(Patient patient: patients.values()){
            allPatients.put(patient.getSocialSecurityNumber(), patient);
        }
        return allPatients;
    }
    /**
     * Method for editing a patient.
     * @param newFirstName
     * @param newLastName
     * @param socialSecurityNumber cannot be edited because a person can't change his/her social
     *                             security number.
     * @param newDiagnosis
     * @param newGeneralPractitioner
     */
    public void editPatient(String newFirstName, String newLastName, String socialSecurityNumber,
                            String newDiagnosis, String newGeneralPractitioner) {
        if(newFirstName.equals("") || newLastName.equals("")){
            throw new NullPointerException("Name cannot be null");
        }else {
            patients.replace(socialSecurityNumber, new Patient(newFirstName, newLastName, socialSecurityNumber, newDiagnosis, newGeneralPractitioner));
        }
    }
}
