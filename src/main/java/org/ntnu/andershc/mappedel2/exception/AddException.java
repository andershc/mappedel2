package org.ntnu.andershc.mappedel2.exception;
/**
 * AddException class
 * Used when trying to add a patient that already exists in the register.
 */
public class AddException extends Exception{
    private static final long serialVersionUID = 1L;

    /**
     * @param socialSecurityNumber of the person that doesn't exist in the register.
     */
    public AddException(String socialSecurityNumber){
        super(socialSecurityNumber);
    }
}
