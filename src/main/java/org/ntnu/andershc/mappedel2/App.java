package org.ntnu.andershc.mappedel2;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import org.ntnu.andershc.mappedel2.model.Patient;
import org.ntnu.andershc.mappedel2.model.PatientRegister;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

/**
 * JavaFX App
 */
public class App extends Application {
    private static PatientRegister register = new PatientRegister();
    private static ObservableList<Patient> registerWrapper;

    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage stage) throws IOException {
        Scene scene = new Scene(loadFXML("main"), 707, 480);
        stage.setScene(scene);
        stage.setTitle("Patient Register v0.1-SNAPSHOT");
        stage.show();
        stage.setOnCloseRequest(event -> {
            alertOnExit();
            event.consume();
        });
    }

    /**
     * Loads an fxml file.
     * @param fxml
     * @return The fxml file
     * @throws IOException if the file is not found.
     */
    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    /**
     * Sets the register of the application
     * @param register
     */
    public static void setRegister(PatientRegister register){
        App.register = register;
    }

    public static PatientRegister getRegister(){
        return register;
    }

    /**
     * Updates the register wrapper.
     */
    public static void updateWrapper(){
        registerWrapper = FXCollections.observableList(new ArrayList<>(register.getAllPatients().values()));
    }

    public static ObservableList<Patient> getWrapper() {
        return registerWrapper;
    }

    /**
     * Resets the register wrapper, clears all data.
     */
    public static void resetWrapper(){
        registerWrapper.clear();
    }

    /**
     * Shows a confirmation alert when trying to close the application.
     */
    public static void alertOnExit() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation Dialog");
        alert.setContentText("Do you want to exit the application?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
            System.exit(1);
        }
    }
}