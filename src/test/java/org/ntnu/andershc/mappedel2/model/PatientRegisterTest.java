package org.ntnu.andershc.mappedel2.model;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.ntnu.andershc.mappedel2.exception.AddException;
import org.ntnu.andershc.mappedel2.exception.RemoveException;

import static org.junit.jupiter.api.Assertions.*;

public class PatientRegisterTest {
    PatientRegister register = new PatientRegister();
    /**
     * Tests for adding a patient
     */
    @Nested
    public class addPatientTest{
        @Test
        public void addPatientTestPositive() throws AddException {
            register.addPatient("test", "testesen", "12111111111",
                    "testDiagnosis", "testPractitioner");
            register.addPatient("test", "testesen", "11111111111",
                    "testDiagnosis", "testPractitioner");
            assertEquals(2, register.getAllPatients().size());
        }
        @Test
        public void addPatientTestNegative() throws AddException {
            register.addPatient("test", "testesen", "12111111111",
                    "testDiagnosis", "testPractitioner");
            assertThrows(AddException.class, () -> register.addPatient("test", "testesen",
                    "12111111111", "testDiagnosis", "testPractitioner"));
        }
        @Test
        public void addPatientTestWrongSSN(){
            assertThrows(IllegalArgumentException.class, () -> register.addPatient("test",
                    "testesen", "aaa", "testDiagnosis",
                    "testPractitioner"));
        }
    }

    /**
     * Tests for removing a patient
     */
    @Nested
    public class removePatientTest{
        @Test
        public void removePatientTestPositive() throws RemoveException, AddException {
            Patient patient = new Patient("test", "testesen", "12111111111",
                    "testDiagnosis", "testPractitioner");
            register.addPatient("test", "testesen", "12111111111",
                    "testDiagnosis", "testPractitioner");
            register.remove(patient.getSocialSecurityNumber());
            assertEquals(0, register.getAllPatients().size());
        }
        @Test
        public void removePatientTestNegative() {
            Patient patient = new Patient("test", "testesen", "12111111111",
                    "testDiagnosis", "testPractitioner");
            assertThrows(RemoveException.class, () -> register.remove(patient.getSocialSecurityNumber()));
        }
    }

    /**
     * Tests for editing a patients details
     */
    @Nested
    public class editPatientTest{
        @Test
        public void editPatientTestPositive() throws AddException {
            Patient editPatient = new Patient("newFirstName", "newLastName",
                    "12111111100", "newDiagnosis", "newPractitioner");
            register.addPatient("test", "testesen", "12111111100",
                    "testDiagnosis", "testPractitioner");
            register.editPatient(editPatient.getFirstName(), editPatient.getLastName(),
                    editPatient.getSocialSecurityNumber(), editPatient.getDiagnosis(),
                    editPatient.getGeneralPractitioner());
            assertEquals(editPatient, register.getAllPatients().get("12111111100"));
        }
        @Test
        public void editPatientTestNegative() throws AddException {
            register.addPatient("test", "testesen", "12111111100",
                    "testDiagnosis", "testPractitioner");
            assertThrows(NullPointerException.class, () -> register.editPatient("", "",
                    "12111111100", "newDiagnosis", "newPractitioner"));
        }
    }
}