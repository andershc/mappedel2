package org.ntnu.andershc.mappedel2.fileHandling;

import org.junit.jupiter.api.Test;
import org.ntnu.andershc.mappedel2.model.PatientRegister;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests for the Reader class.
 */
public class ReaderTest {
    PatientRegister register = new PatientRegister();

    /**
     * Positive test for the readCSV method.
     * Uses a pre defined .csv file as test data:
     * src/main/resources/dataCSV/testRead.csv¨.
     */
    @Test
    public void readCSVTestPositive(){
        Reader reader = new Reader("src/main/resources/dataCSV/testRead.csv");
        reader.readCSV(register);
        assertEquals(2, register.getAllPatients().size());
    }

    /**
     * Negative test for the readCSV method. takes in a non-.csv file.
     */
    @Test
    public void readCSVTestNegative(){
        Reader reader = new Reader("NotAPath");
        assertThrows(IllegalArgumentException.class, () -> reader.readCSV(register));
    }

}