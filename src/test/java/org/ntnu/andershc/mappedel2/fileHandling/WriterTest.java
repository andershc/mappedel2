package org.ntnu.andershc.mappedel2.fileHandling;

import org.junit.jupiter.api.Test;
import org.ntnu.andershc.mappedel2.exception.AddException;
import org.ntnu.andershc.mappedel2.model.PatientRegister;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests for the Writer class.
 */
public class WriterTest {
    PatientRegister register = new PatientRegister();

    /**
     * Positive test for the writeRegisterCSV method.
     * @throws AddException
     */
    @Test
    public void writeRegisterCSVTestPositive() throws AddException {
        Writer writer = new Writer("src/main/resources/dataCSV/testWrite.csv");
        Reader reader = new Reader("src/main/resources/dataCSV/testWrite.csv");

        register.addPatient("test", "tester", "11111111112",
                "tested", "testing");
        register.addPatient("test", "tester", "11111111113",
                "tested", "testing");

        writer.writeRegisterCSV(register);
        PatientRegister register2 = new PatientRegister();
        reader.readCSV(register2);

        assertEquals(register.getAllPatients(), register2.getAllPatients());
    }
    /**
     * Negative test for the writeRegisterCSV method, takes in a non-.csv file.
     * @throws AddException
     */
    @Test
    public void writeRegisterCSVTestNegative(){
        Writer writer = new Writer("notACSVFile");
        assertThrows(IllegalArgumentException.class, () -> writer.writeRegisterCSV(register));
    }

}